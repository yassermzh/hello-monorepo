const express = require('express')
const app = express()
const port = 3000

const logRequestStart = (req, res, next) => {
    console.info(`${req.method} ${req.originalUrl}`)
    next()
}

app.use(logRequestStart)

app.get('/', (req, res) => res.send('alpha: Hello World!'))

app.listen(port, () => console.log(`alpha: Example app listening on port ${port}!`))

