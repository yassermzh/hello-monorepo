#!/bin/bash -e
file=$1

trigger_build="false"
#echo "commit=$GIT_COMMIT"
#echo "previous commit=$GIT_PREVIOUS_COMMIT"
detect_changed() {
  folders=`git diff --name-only $GIT_COMMIT $GIT_PREVIOUS_COMMIT | sort -u | uniq`
  #echo "folders=$folders"
  export tochanged="${folders}"
}

run_tests() {
  for component in $tochanged; do
    if echo $component | grep -q $file; then
      #echo "$component has changed"
      #echo "triggering build"
      trigger_build="true"
      break 3
    fi
  done
}

detect_changed
run_tests

echo $trigger_build
